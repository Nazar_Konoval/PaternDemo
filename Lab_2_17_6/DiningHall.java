package com.breinacad.M2.Lab_2_17_6;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class DiningHall {
    static int pizzaNum;
    static int studentID = 1;

    public synchronized void makePizza() {
       if (pizzaNum >10) {
           try{
               TimeUnit.SECONDS.sleep(5_000);
           } catch (InterruptedException e){
               e.printStackTrace();
           }
       } else {

       }
    }

    public synchronized void servePizza() {
        String result;
        if (pizzaNum > 0) {
            result = "Served ";
            pizzaNum--;
        } else result = "Starved ";
        System.out.println(result + studentID);
        studentID++;
    }
    public static void main(String[] args) {
        DiningHall d = new DiningHall();
//        for (int i = 0; i < 10; i++)
//            d.makePizza();
//        for (int i = 1; i <= 20; i++)
//            d.servePizza();
        Semaphore semaphore = new Semaphore(10);
        for (int i = 0; i < 20 ; i++) {
            DiningHall.Student sublent = new DiningHall.Student(semaphore, d);
        }

    }
    static class Student extends Thread {
        private Semaphore semaphore;
        private DiningHall diningHall;

        public Student(Semaphore semaphore, DiningHall diningHall){
            this.semaphore = semaphore;
            this.diningHall = diningHall;
        }

        @Override
        public void run() {
            super.run();
        }
    }

}
