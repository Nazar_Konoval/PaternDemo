package com.breinacad.M2.Lab_2_18;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class Main2 {
    public static void main(String[] args) {

        Properties properties = new Properties();
        properties.setProperty("name", "Nazar");
        properties.setProperty("phone", "389984");
        try {
            properties.store(new FileWriter("myconfig.properties"), "Coment to my config");
        } catch (IOException e){
                e.printStackTrace();
        }

        Properties properties1 = new Properties();
        try{
            properties1.load(new FileReader("myconfig.properties"));
        }catch (IOException e){
            e.printStackTrace();
        }

        System.out.println(properties1.getProperty("user.name"));
        System.out.println(properties1.getProperty("user.phone"));

        //JSON
        //INI
        //properties
        //xml
        //HOCON, TypeSafeConfig
    }
}
