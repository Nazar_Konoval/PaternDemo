package com.breinacad.M2.Lab_2_18;


import java.io.IOException;
import java.util.Random;

public class Lab_18_4 {
    public static void main(String[] args) {
        Runtime rt = Runtime.getRuntime();
        String[] programs = new String[]{"evince", "gedit"};
        Random random = new Random();
        int id = random.nextInt(programs.length);
        try{
            System.out.println("run: " + programs[id]);
            rt.exec(programs[id]);
    } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
