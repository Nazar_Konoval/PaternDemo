package com.breinacad.M2.Lab_2_18;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

public class L18_3 {
    public static void main(String[] args) {
        //Arrays.asList(Locale.getAvailableLocales()).forEach(System.out::println); коди країн
       // Currency.getAvailableCurrencies().forEach(System.out::println); коди грошей
       // System.out.println(Locale.getDefault());
        System.out.println(Locale.getDefault());

        Locale ruLocale = new Locale("ru", "RU");
        long val2 = 2_430_000_000_000L;

        System.out.printf("%s (%s)\n", ruLocale.getDisplayCountry(),
                ruLocale.getDisplayLanguage());

        NumberFormat ruNumberFormat = NumberFormat.getNumberInstance(ruLocale);
        double val = 5.900_900_500;

        Date date = new Date();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("GG yyyy MM dd HH:mm:ss", ruLocale);

        System.out.println(date);

        System.out.println(ruNumberFormat.format(val));
        System.out.println(ruNumberFormat.format(val2));
        System.out.println(NumberFormat.getCurrencyInstance(ruLocale).format(val2));
        System.out.println(NumberFormat.getCurrencyInstance(ruLocale).format(val));
        //System.out.println(dateTimeFormatter.format(date.toInstant()));

        /// ЗМІНИТИ ЛОКАЛІ НА КИТАЙ І ІТАЛІЮ ЯК В ЛАБ
    }
}
