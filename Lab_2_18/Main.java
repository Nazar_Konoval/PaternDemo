package com.breinacad.M2.Lab_2_18;

import java.util.Arrays;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        Map<String, String> env = System.getenv();
        for (String envName : env.keySet()) {
            System.out.format("%s=%s%n",
                    envName,
                    env.get(envName));
             }

        System.out.println("\n" +System.getenv("MyName"));
        System.out.println(Arrays.toString(System.lineSeparator().getBytes()));
    }
}
