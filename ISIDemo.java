package com.breinacad.M2;


import java.io.Serializable;

abstract class Animal{
    int age = 5;
    private String name;
    abstract public void eat();

    public  Animal (String name) {
        this.name = name;
        System.out.println("Constructor of Animal " + name);
    }
}

class Dog extends Animal {
    int age =10;
    public Dog(String name){
        super(name);
        System.out.println("Constructor of Dog " + name);
    }
    @Override
    public void eat() {
        System.out.println("dog eat ");
    }

}
class Boxer extends Dog{
    int age = 3;
    public Boxer(String name) {
        super(name);
        System.out.println("Construcrot of Boxer " + name);
    }

    @Override
    public void eat() {
        System.out.println("Boxer is eating");
    }
}

interface  God extends Serializable{
    void create();
}

class Human extends Animal implements God {
    public Human(String name) {
        super(name);
    }

    @Override
    public void eat() {
    }

    @Override
    public void create(){
    }
}

public class ISIDemo {
    int a;

    public static void main(String[] args) {
    Animal animal = new Boxer("Rex");

    animal.eat();

        System.out.println(animal.age);



    }
}
