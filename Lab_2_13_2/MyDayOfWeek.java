package com.breinacad.M2.Lab_2_13_2;

public enum MyDayOfWeek {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRYDAY, SATURDAY, SUNDAY;

    public MyDayOfWeek nextDay() {
        int i = this.ordinal();
        if (i > MyDayOfWeek.values().length) {
            return MyDayOfWeek.MONDAY;
        } else {
            return MyDayOfWeek.values()[++i];
        }

    }

  /*  public MyDayOfWeek nextDay(MyDayOfWeek day) {
        int i = this.ordinal();
        if (i >= MyDayOfWeek.values().length) {
            return MyDayOfWeek.MONDAY;
        } else {
            return MyDayOfWeek.values()[++i];
        }

    }*/
}

