package com.breinacad.M2.LAB_2_13_4;

public class Main {
    public static void main(String[] args) {
        Card[] cards = new Card[Suit.values().length * Rank.values().length];
        int idx = 0;

        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                Card card = new Card(suit, rank);
                cards[idx++] = card;
                System.out.println(cards[--idx]);
          //      System.out.println(card);
            }
        }
    }
}
