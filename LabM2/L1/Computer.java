package com.breinacad.M2.LabM2.L1;

public class Computer {

    private String manufacture;
    //   private int serialNumber;
    private float price;
//   private int quantity;
//   private int frequencyCPU;

    public Computer(String manufacture) {
        this.manufacture = manufacture;
    }

    public String getManufacture() {
        return manufacture;
    }

    public void setManufacture(String manufacture) {
        this.manufacture = manufacture;
    }
    //
//    public int getSerialNumber() {
//        return serialNumber;
//    }
//
//    public void setSerialNumber(int serialNumber) {
//        this.serialNumber = serialNumber;
//    }
//
    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    //    public int getQuantity() {
//        return quantity;
//    }
//
//    public void setQuantity(int quantity) {
//        this.quantity = quantity;
//    }
//
//    public int getFrequencyCPU() {
//        return frequencyCPU;
//    }
//
//    public void setFrequencyCPU(int frequencyCPU) {
//        this.frequencyCPU = frequencyCPU;
//    }
    @Override
    public String toString() {
        return "Computer{" +
                "manufacture='" + manufacture + '\'' +
                ", price=" + price +
                '}';
    }
}