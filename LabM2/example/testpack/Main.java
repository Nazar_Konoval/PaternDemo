package com.breinacad.M2.LabM2.example.testpack;

import com.breinacad.M2.LabM2.example.carpack.Car;
import com.breinacad.M2.LabM2.example.applepack.Apple;

public class Main {
    public static void main(String[] args) {
        Apple apple=new Apple();
        Car car=new Car();
        apple.setVeight(0.2);
        car.setVeight(1500);
        System.out.println("apple: "+apple.getVeight());
        System.out.println("car: "+car.getVeight());
    }
}
