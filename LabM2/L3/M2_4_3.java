package com.breinacad.M2.LabM2.L3;

import java.util.Arrays;
import java.util.SortedMap;

public class M2_4_3 {
    public static final double Piii=Math.PI;
    public static double arreaOfCircln (double r){
        return Piii*r*r;
    }


    public static int findMin (int[]arr){

        int min=arr[0];
        for (int i=0; i<arr.length; i++) {
            if (arr[i] < min) min = arr[i];
        }
        return min;
    }

    public static void main(String[] args) {
        int[] arr=new int[10];

        for (int i=0; i<10; i++){
            arr[i]=(int)(Math.random()*10-12);

        }
        System.out.println(Arrays.toString(arr));
        System.out.println(findMin(arr));
        System.out.println(arreaOfCircln(3));
    }
}

