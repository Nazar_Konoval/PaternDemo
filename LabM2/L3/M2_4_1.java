package com.breinacad.M2.LabM2.L3;


import java.util.Arrays;

public class M2_4_1 {
   public static int findMin (int[]arr){

       int min=arr[0];
       for (int i=0; i<arr.length; i++) {
           if (arr[i] < min) min = arr[i];
       }
           return min;
   }

    public static void main(String[] args) {
        int[] arr=new int[10];

        for (int i=0; i<10; i++){
            arr[i]=(int)(Math.random()*10-12);

        }
        System.out.println(Arrays.toString(arr));
           System.out.println(findMin(arr));
   }
}
