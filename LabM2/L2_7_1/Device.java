package com.breinacad.M2.LabM2.L2_7_1;

class Device {
    String manufacrure;
    float price;
    String serialNumber;

    public void setManufacrure(String manufacrure) {
        this.manufacrure = manufacrure;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getManufacrure() {
        return manufacrure;
    }

    public float getPrice() {
        return price;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public Device(String manufacrure, float price, String serialNumber) {
        this.manufacrure = manufacrure;
        this.price = price;
        this.serialNumber = serialNumber;
    }

    @Override
    public String toString() {
        return "Device{" +
                "manufacrure='" + manufacrure + '\'' +
                ", price=" + price +
                ", serialNumber='" + serialNumber + '\'' +
                '}';
    }
}
