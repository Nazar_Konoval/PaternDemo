package com.breinacad.M2.LabM2.L2_7_1;

class Monitor extends Device {
    int resultionX;
    int resultionY;

    public void setResultionX(int resultionX) {
        this.resultionX = resultionX;
    }

    public void setResultionY(int resultionY) {
        this.resultionY = resultionY;
    }

    public int getResultionX() {
        return resultionX;
    }

    public int getResultionY() {
        return resultionY;
    }

    public Monitor(String manufacrure, float price, String serialNumber, int resultionX, int resultionY) {
        super(manufacrure, price, serialNumber);
        this.resultionX = resultionX;
        this.resultionY = resultionY;
    }

    @Override
    public String toString() {
        return "Monitor{" +
                "manufacrure='" + manufacrure + '\'' +
                ", price=" + price +
                ", serialNumber='" + serialNumber + '\'' +
                ", resultionX=" + resultionX +
                ", resultionY=" + resultionY +
                '}';
    }
}
