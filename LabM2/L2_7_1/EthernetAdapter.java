package com.breinacad.M2.LabM2.L2_7_1;

class EthernetAdapter extends Monitor{
    int Speed;
    String mac;

    public void setSpeed(int speed) {
        Speed = speed;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public int getSpeed() {
        return Speed;
    }

    public String getMac() {
        return mac;
    }

    public EthernetAdapter(String manufacrure, float price, String serialNumber, int resultionX, int resultionY, int speed, String mac) {
        super(manufacrure, price, serialNumber, resultionX, resultionY);
        Speed = speed;
        this.mac = mac;
    }


}
