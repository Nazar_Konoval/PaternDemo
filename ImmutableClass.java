package com.breinacad.M2;

import com.sun.org.apache.xpath.internal.operations.String;

final class MyImmutableClass{
    private String field;

    public MyImmutableClass(String field) { this.field = field; }

    public String getField() { return field; }

    public MyImmutableClass changeMyImmutable(String newFieldValue){
        return new MyImmutableClass(newFieldValue);
    }
}

public class ImmutableClass {

}
