package com.breinacad.M2.Lab_2_16_2;

import com.breinacad.M2.Lab_2_16_1.Animal;
import com.breinacad.M2.Lab_2_16_1.Dog;

public class Main {
    public static void main(String[] args) {
       byte b1 = 1;
       byte b2 = 2;

       food(b1);
       food(b2);
       food(b2, b2);

    }

//    public static void(){
//        ...
//    }

    public static void food(byte...b){
        System.out.println("varargs");
    }

    public static void food(Byte b){
        System.out.println("Byte");

    }
    public static void m(byte b){
        System.out.println("byte");
    }

}
