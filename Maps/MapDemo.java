/*
package com.breinacad.M2.Maps;

import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.Map;


public class MapDemo {
    public static void main(String[] args) {
        Map<String, String> phoneBook = new HashMap<>();
        phoneBook.put("Ivan", "(099)555-555");
        phoneBook.put("Andriy", "(099)555-555");
        phoneBook.put("Nazar", "(099)555-555");
        phoneBook.put("Viktor", "(099)555-555");

        System.out.println(phoneBook);
       */
/* Set<String> names = phoneBook.keySet();
        System.out.println(names);
        System.out.println();
        Collection<String > values = phoneBook.values();

        Set<Map.Entry<String, String>> entry = phoneBook.entrySet();
        entry.forEach(System.out::println);

        System.out.println();
        for(Map.Entry<String, String> entryPB : phoneBook.entrySet()){
            System.out.println(entryPB);*//*


        Map<String, String> orderedPhoneBook =
                //             new TreeMap<>(Comparator.comparing(String::length));
                new TreeMap<>(new Comparator<String>() {
                    @Override
                    public int compare(String o1;String o2) {
                        return Integer.compare(o1.length(), o2.length());

                    }
                });
        orderedPhoneBook.putAll(phoneBook);
        System.out.println(orderedPhoneBook);

        List<String> strings;
        try {
            strings = Files.readAllLines(new File("hello.txt").toPath());
        } catch (IOException ioe) {
            ioe.printStackTrace();
            strings = Collections.emptyList();

        }
        strings.forEach(System.out::println);
    }


    }

*/
